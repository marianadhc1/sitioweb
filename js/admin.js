// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import {getFirestore, doc, getDoc, getDocs, collection} from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import{ getDatabase, ref, set, onValue, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import{ getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAPSFxmOUPLX8Mm1iOP9RYc1wespa1uZMs",
  authDomain: "appweb-edd18.firebaseapp.com",
  projectId: "appweb-edd18",
  storageBucket: "appweb-edd18.appspot.com",
  messagingSenderId: "488869158151",
  appId: "1:488869158151:web:0648eca90dd7cd96e5b9a2"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

//Declaración de objetos 
var btnGuardar = document.getElementById('guardar');
var btnActualizar = document.getElementById('actualizar');
var btnConsultar = document.getElementById('consultar');
var btnDeshabilitar = document.getElementById('deshabilitar');
var btnMostrar = document.getElementById('mostrar');

var archivo = document.getElementById('archivo');
var lista = document.getElementById('lista');
var contenidoProductos = document.getElementById('contenidoProductos');
var seccionProductos = document.getElementById('seccionProductos');

//Insertar 
var codigo=""; 
var nombre="";
var descripcion="";
var precio=""; 
var url=""; 
var nombreImg="";
var status="";
//status.innerHTML = "";

window.onload = mostrarProductos();

function leerInputs(){
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    url = document.getElementById('url').value;
    nombreImg = document.getElementById('nombreImg').value;
    //status = document.getElementById('status').value;
}

function escribirInputs(){
    document.getElementById('codigo').value = codigo;
    document.getElementById('nombre').value = nombre;
    document.getElementById('precio').value = precio;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('url').value = url;
    document.getElementById('nombreImg').value = nombreImg;
    document.getElementById('archivo').url = archivo;
    //document.getElementById('status').value = status; 
}

function cargarImagen(){
    const file = event.target.files[0];
    const name = event.target.files[0].name;

    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);

    uploadBytes(storageRef, file)
    .then((snapshot)=> {
        document.getElementById('nombreImg').value = name; 
        alert('Se cargó la imagen');

        setTimeout(descargarImagen,2000);
    });
}

function descargarImagen(){
    archivo = document.getElementById('nombreImg').value; 

    const storage = getStorage(); 
    const starsRef = refS(storage, 'imagenes/' + archivo); 

    getDownloadURL(starsRef)
    .then((url)=>{
        document.getElementById('url').value = url; 
        document.getElementById('imagen').src = url; 
    })
    .catch((error)=>{
        switch (error.code) {
            case 'storage/object-not-found':
              alert("No existe el archivo");
            break;
            case 'storage/unauthorized':
              alert("No tiene permisos");
            break;
            case 'storage/canceled':
              alert("Se canceló o no tiene internet");
            break;
            // ...
            case 'storage/unknown':
              alert("NO SÉ QUE PASO :(");
            break;
        }
    });
}

function mostrarProductos(){
    if(lista!=null){
        lista.innerHTML="";
    }
    const dbref = ref(db, 'productos/');

    onValue(dbref,(snapshot)=>{
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key; 
            const childData = childSnapshot.val();

            if(lista){
                if(childData.status == 0){
                    lista.innerHTML = lista.innerHTML + 
                    "<div class='caja'><img src='" + childData.url + "' class='imgProductos'><div class='descripcion'><h4> (" 
                    + childKey + ") "+ childData.nombre + " $" + childData.precio + "</h4><p>" + childData.descripcion + "<br> Estatus: "+ childData.status + "</p></div></div>";
                }
                if(childData.status == 1){
                    lista.innerHTML = lista.innerHTML + 
                    "<div class='caja'><img src='" + childData.url + "' class='imgProductos'><div class='descripcion'><h4> (" 
                    + childKey + ") "+ childData.nombre + " $" + childData.precio + "</h4><p>" + childData.descripcion + "<br> Estatus: "+ childData.status + "</p></div></div>";
                }
            }
            if(contenidoProductos){
                if(childData.status == 0){
                    contenidoProductos.innerHTML = contenidoProductos.innerHTML + 
                    "<div class='caja'><img src='" + childData.url + "' class='imgProductos'><div class='descripcion'><h4>" 
                    + childData.nombre + " $" + childData.precio + "</h4><p>" + childData.descripcion + "</p></div></div>"
                }
            }
        });
    },{
        onlyOnce: true
    });
}

function guardar(){
    leerInputs(); 

    set(ref(db, 'productos/' + codigo),{
        nombre:nombre,
        descripcion:descripcion,
        precio:precio,
        url:url,
        nombreImg:nombreImg,
        status:0,
    })
    .then((response)=>{
        alert("Se agregó el producto con éxito");
        mostrarProductos();
        limpiar();
    })
    .catch((error)=>{
        alert("Surgió un error" + error);
    })    
}

function buscar(){
    leerInputs();
    const dbref = ref(db);
    seccionProductos.innerHTML="";

    get(child(dbref, 'productos/' + codigo))
    .then((snapshot)=>{
        if(snapshot.exists()){
            nombre = snapshot.val().nombre,
            descripcion = snapshot.val().descripcion,
            precio = snapshot.val().precio,
            url = snapshot.val().url,
            nombreImg = snapshot.val().nombreImg
            escribirInputs();
            seccionProductos.innerHTML = seccionProductos.innerHTML + "<img id='imagen' src='" + url + "'alt='Productos' width='150px'>";
        }else{
            alert("No existe ese producto");
        }
    })
    .catch((error)=>{
        alert("Surgió un error " + error);

        seccionProductos.innerHTML = seccionProductos.innerHTML + "<img id='imagen' src='" + url + "'alt='Productos' width='150px'>";
    });
}

function actualizar(){
    leerInputs();
    update(ref(db, 'productos/' + codigo),{
        nombre:nombre,
        descripcion:descripcion,
        precio:precio,
        url:url,
        nombreImg:nombreImg,
        status:0
    })
    .then(()=>{
        alert("Se realizó la actualización con éxito");
        mostrarProductos();
        limpiar();
    })
    .catch(()=>{
        alert("Surgio un error " + error);
    })
}

function deshabilitar(){
    leerInputs();
    update(ref(db, 'productos/' + codigo),{
        nombre:nombre,
        descripcion:descripcion,
        precio:precio,
        url:url,
        nombreImg:nombreImg,
        status:1
    })
    .then(()=>{
        alert("Se deshabilitó el producto con éxito");
        mostrarProductos();
        limpiar();
    })
    .catch((error)=>{
        alert("Surgio un error " + error);
    })
}

function limpiar(){
    codigo = "";
    nombre = "";
    precio = "";
    descripcion = "";
    nombreImg = "";
    url = "";
    status="";
    escribirInputs();
    document.getElementById('url').value = "";
    document.getElementById('nombreImg').value ="";  
}

if(btnGuardar){
    btnGuardar.addEventListener('click', guardar);
}
if(btnConsultar){
    btnConsultar.addEventListener('click', buscar);
}
if(btnActualizar){
    btnActualizar.addEventListener('click', actualizar);
}
if(btnMostrar){
    btnMostrar.addEventListener('click', mostrarProductos);
}
if(archivo){
    archivo.addEventListener('change', cargarImagen);
}
if(btnDeshabilitar){
    btnDeshabilitar.addEventListener('click', deshabilitar);
}

//Eventos click 
//btnGuardar.addEventListener('click', guardar);
//archivo.addEventListener('change', cargarImagen);
//btnConsultar.addEventListener('click', buscar);
//btnActualizar.addEventListener('click', actualizar);
//btnMostrar.addEventListener('click', mostrarProductos);
//btnDeshabilitar.addEventListener('click', deshabilitar);
