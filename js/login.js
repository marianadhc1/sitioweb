import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword   } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyAPSFxmOUPLX8Mm1iOP9RYc1wespa1uZMs",
  authDomain: "appweb-edd18.firebaseapp.com",
  projectId: "appweb-edd18",
  storageBucket: "appweb-edd18.appspot.com",
  messagingSenderId: "488869158151",
  appId: "1:488869158151:web:0648eca90dd7cd96e5b9a2"
};

const app = initializeApp(firebaseConfig);

var btnCrear = document.getElementById('crearUsuario');
var btnLogin = document.getElementById('iniciarSesion');
var btnLimpiar = document.getElementById('limpiar');

var email="";
var pass="";

function iniciarSesion(){
  var email = document.getElementById('email').value;
  var pass = document.getElementById('pass').value;
  const auth = getAuth();

  signInWithEmailAndPassword (auth, email, pass)
  .then((userCredential)=>{
    //Signed in 
    alert("Se inició sesión")
    window.location.href = "../proyecto/html/admin.html";
  })
  .catch((error)=>{
    const errorCode = error.code; 
    const errorMessage = error.message; 
    alert("Ocurrió un error " + errorMessage)
  });
}

function escribirInputs(){
  document.getElementById('email').value = email;
  document.getElementById('pass').value = pass;
}


function limpiar(){
  email = "";
  pass = "";
  escribirInputs();
}

//Eventos click
//btnCrear.addEventListener('click', crearUsuario);
btnLogin.addEventListener('click', iniciarSesion);
btnLimpiar.addEventListener('click', limpiar);